const {response,request} = require('express');

const usuariosGet = (req=request,res = response) => {
  const query = req.query;

  res.json({
    msg: 'GET api - controlador',
    query
  })
}

const usuariosPost = (req,res=response) => {
  const body = req.body;

  res.json({
    msg: 'POST api - controlador',
    body
  })
}

const usuariosPut = (req,res=response) => {
  const id = req.params.id;
  res.json({
    msg: 'PUT api - controlador',
    id
  })
}

const usuariosPatch = (req,res=response) => {
  res.json({
    msg: 'PATCH api - controlador'
  })
}

const usuariosDelete = (req,res=response) => {
  res.json({
    msg: 'DELETE api - controlador'
  })
}

module.exports = {
  usuariosGet,
  usuariosPost,
  usuariosPut,
  usuariosPatch,
  usuariosDelete 
}